﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereController : MonoBehaviour {

    private Rigidbody rb;

    private enum Direction
    {
        Up,
        Back,
        Right,
        Front,
        Left,
        Down
    }

    public float acceleration;
    
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate () {
        Direction direction = (Direction)Random.Range((int)Direction.Up, (int)Direction.Down + 1);

        switch (direction)
        {
            case Direction.Up:
                rb.AddForce(0, acceleration, 0);
                break;

            case Direction.Back:
                rb.AddForce(0, 0, -acceleration);
                break;

            case Direction.Right:
                rb.AddForce(acceleration, 0, 0);
                break;

            case Direction.Front:
                rb.AddForce(0, 0, acceleration);
                break;

            case Direction.Left:
                rb.AddForce(-acceleration, 0, 0);
                break;

            case Direction.Down:
                rb.AddForce(0, -acceleration, 0);
                break;

            default:
                break;
        }
	}
}
